package ru.sadkov.tm.util;

import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.api.ProjectMapper;
import ru.sadkov.tm.api.SessionMapper;
import ru.sadkov.tm.api.TaskMapper;
import ru.sadkov.tm.api.UserMapper;

import javax.sql.DataSource;

@UtilityClass
public class MyBatisUtil {

    @Nullable
    private static final String USER = "root";
    @Nullable
    private static final String PASSWORD = "root";
    @Nullable
    private static final String URL = "jdbc:mysql://localhost:3306/app_db";
    @Nullable
    private static final String DRIVER = "com.mysql.jdbc.Driver";


    @SneakyThrows
    public static SqlSessionFactory getSqlSessionFactory() {
        Class.forName(DRIVER);
        final DataSource dataSource =
                new PooledDataSource(DRIVER, URL, USER, PASSWORD);
        final TransactionFactory transactionFactory =
                new JdbcTransactionFactory();
        final Environment environment =
                new Environment("development", transactionFactory, dataSource);
        final Configuration configuration = new Configuration(environment);
        configuration.addMapper(UserMapper.class);
        configuration.addMapper(SessionMapper.class);
        configuration.addMapper(ProjectMapper.class);
        configuration.addMapper(TaskMapper.class);
        return new SqlSessionFactoryBuilder().build(configuration);
    }
}
