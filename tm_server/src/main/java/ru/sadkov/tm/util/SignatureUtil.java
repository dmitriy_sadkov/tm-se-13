package ru.sadkov.tm.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.security.NoSuchAlgorithmException;

@UtilityClass
public final class SignatureUtil {

    @Nullable
    public static String sign(@Nullable final Object value, @Nullable final String salt, @Nullable final Integer cycle) {
        if (value == null) return null;
        if (salt == null || salt.isEmpty()) return null;
        if (cycle == null) return null;
        try {
            @NotNull final ObjectMapper objectMapper = new ObjectMapper();
            @NotNull final String json = objectMapper.writeValueAsString(value);
            return sign(json, salt, cycle);
        } catch (JsonProcessingException | NoSuchAlgorithmException e) {
            return null;
        }
    }

    public static String sign(@NotNull final String json, @NotNull final String salt, @NotNull final Integer cycle) throws NoSuchAlgorithmException {
        @Nullable String result = json;
        for (int i = 0; i < cycle; i++) {
            result = HashUtil.hashMD5(salt + result + salt);
        }
        return result;
    }
}
