package ru.sadkov.tm.api;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import ru.sadkov.tm.entity.Session;

import java.util.List;

public interface SessionMapper {

    @NotNull
    @Select("SELECT * FROM session WHERE user_id = #{userId}")
    @Results(value = {
            @Result (property = "id", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "timeStamp", column = "timestamp"),
            @Result(property = "role", column = "role"),
            @Result(property = "signature", column = "signature")
    })
    List<Session> findByUserId(@NotNull final String userId);

    @Delete("DELETE FROM session WHERE id = #{id}")
    void close(@NotNull final String id);

    @Select("SELECT * FROM session")
    @Results(value = {
            @Result (property = "id", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "timeStamp", column = "timestamp"),
            @Result(property = "role", column = "role"),
            @Result(property = "signature", column = "signature")
    })
    List<Session> findAll()  ;

    @Insert("INSERT INTO session (id, user_id, timestamp, role, signature) VALUES(#{id},#{userId},#{timeStamp},#{role},#{signature})")
    void persist(@NotNull Session session);

    @Delete("DELETE FROM session")
    void clear();

    @NotNull
    @Select("SELECT COUNT(*) FROM session")
    Long count();

    @NotNull
    @Select("SELECT COUNT(*) = 0 FROM session")
    Boolean isEmpty();

    @NotNull
    @Select("SELECT COUNT(*) > 0 FROM session WHERE id = #{id}")
    Boolean contains(@NotNull final String id);
}
