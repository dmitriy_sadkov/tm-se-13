package ru.sadkov.tm.api;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.entity.User;

import java.util.List;

public interface UserMapper {

    @Nullable
    @Select("SELECT * FROM user WHERE id = #{userId}")
    User findOne(@NotNull final String userId);

    @Nullable
    @Select("SELECT * FROM user WHERE login = #{login}")
    User findByLogin(@NotNull final String login);

    @NotNull
    @Select("SELECT * FROM user WHERE role = #{role}")
    List<User> findAllByRole(String role);

    @NotNull
    @Select("SELECT * FROM user")
    List<User> findAll();

    @Delete("DELETE FROM user")
    void removeAll();

    @Update("UPDATE user SET login = #{login} where id = #{id}")
    void update(@Param("id") @NotNull final String id, @Param("login") @NotNull final String login);

    @Delete("DELETE FROM user WHERE login = #{login}")
    void removeByLogin(@NotNull final String login);

    @Insert("INSERT INTO user (id, login, password, role) VALUES(#{id},#{login},#{password},#{role})")
    void persist(@Param("id") @NotNull final String id,
                 @Param("login") @NotNull final String login,
                 @Param("password") @NotNull final String password,
                 @Param("role") @NotNull final String role);

    @NotNull
    @Select("SELECT COUNT(*) FROM user")
    Long count();

    @NotNull
    @Select("SELECT COUNT(*) = 0 FROM user")
    Boolean isEmpty();

    @NotNull
    @Select("SELECT COUNT(*) > 0 FROM user WHERE id = #{id}")
    Boolean contains(@NotNull final String id);
}
