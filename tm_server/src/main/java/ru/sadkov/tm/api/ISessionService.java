package ru.sadkov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.entity.Session;
import ru.sadkov.tm.entity.User;
import ru.sadkov.tm.enumeration.Role;


import java.sql.SQLException;
import java.util.List;

public interface ISessionService {
    @Nullable
    Session open(@Nullable final String login, @Nullable final String pasword);

    void validate(@Nullable final Session session) throws Exception;

    void validate(@Nullable final Session session, Role role) throws Exception;

    boolean isValid(@Nullable final Session session);

    void close(@Nullable final Session session);

    void closeAll(@Nullable final Session session);

    @Nullable
    List<Session> getListSession(@Nullable final Session session);

    @Nullable
    User getUser(@NotNull final Session session);

    void load(@NotNull final List<Session> sessions);

    @NotNull
    List<Session> findAll();
}
