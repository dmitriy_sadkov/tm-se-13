package ru.sadkov.tm.api;

import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.entity.Project;

import java.util.List;

public interface ProjectMapper {
    @NotNull
    @Select("SELECT * FROM project WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "dateCreate", column = "date_create", jdbcType = JdbcType.DATE),
            @Result(property = "dateBegin", column = "date_begin", jdbcType = JdbcType.DATE),
            @Result(property = "dateEnd", column = "date_end", jdbcType = JdbcType.DATE),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "status", column = "status"),
    })
    List<Project> findAllForUser(@NotNull final String userId);


    @Delete("DELETE FROM project WHERE user_id = #{userId} AND name = #{projectName}")
    void removeByName(@Param("userId")@NotNull final String userId,@Param("projectName") @NotNull final String projectName);


    @Select("SELECT * FROM project WHERE user_id = #{userId} AND name = #{name}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "dateCreate", column = "date_create", jdbcType = JdbcType.DATE),
            @Result(property = "dateBegin", column = "date_begin", jdbcType = JdbcType.DATE),
            @Result(property = "dateEnd", column = "date_end", jdbcType = JdbcType.DATE),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "status", column = "status"),
    })
    @Nullable Project findProjectByName(@Param("name")@NotNull String name,@Param("userId") @NotNull String userId);


    @Delete("DELETE FROM project WHERE user_id = #{userId}")
    void removeAll(@NotNull final String userId);


    @Nullable
    @Select("SELECT * FROM project WHERE user_id = #{userId} AND id = #{projectId}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "dateCreate", column = "date_create", jdbcType = JdbcType.DATE),
            @Result(property = "dateBegin", column = "date_begin", jdbcType = JdbcType.DATE),
            @Result(property = "dateEnd", column = "date_end", jdbcType = JdbcType.DATE),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "status", column = "status"),
    })
    Project findOne(@Param("projectId")@NotNull final String projectId,@Param("userId") @NotNull final String userId);


    @Update("UPDATE project SET name = #{projectName}, description = #{description} WHERE user_id = #{userId} AND id = #{id}")
    void update(@Param("userId")@NotNull final String userId,
                @Param("id") @NotNull final String id,
                @Param("description") @NotNull final String description,
                @Param("projectName") @NotNull final String projectName);


    @NotNull
    @Select("SELECT * FROM project WHERE user_id = #{userId} AND name LIKE %#{part}% or description LIKE %#{part}%")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "dateCreate", column = "date_create", jdbcType = JdbcType.DATE),
            @Result(property = "dateBegin", column = "date_begin", jdbcType = JdbcType.DATE),
            @Result(property = "dateEnd", column = "date_end", jdbcType = JdbcType.DATE),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "status", column = "status"),
    })
    List<Project> findProjectsByPart(@Param("userId")@NotNull final String userId,@Param("part") @NotNull final String part);


    @Update("UPDATE project SET status = #{status}, date_begin = #{dateBegin} WHERE user_id = #{userId} AND name = #{projectName}")
    void startProject(@Param("userId")@NotNull final String userId,@Param("projectName") @NotNull final String projectName,
                      @Param("status")@NotNull final String status, @Param("dateBegin")@NotNull final String dateBegin);


    @Update("UPDATE project SET status = #{status}, date_end = #{dateEnd} WHERE user_id = #{userId} AND name = #{projectName}")
    void endProject(@Param("userId")@NotNull final String userId,@Param("projectName") @NotNull final String projectName,
                    @Param("status")@NotNull final String status,@Param("dateEnd") @NotNull final String dateEnd);


    @NotNull
    @Select("SELECT * FROM project WHERE user_id = #{userId} AND status = #{status}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "dateCreate", column = "date_create", jdbcType = JdbcType.DATE),
            @Result(property = "dateBegin", column = "date_begin", jdbcType = JdbcType.DATE),
            @Result(property = "dateEnd", column = "date_end", jdbcType = JdbcType.DATE),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "status", column = "status"),
    })
    List<Project> findProjectsByStatus(@Param("userId")@NotNull final String userId,@Param("status") @NotNull final String status);


    @NotNull
    @Select("SELECT * FROM project")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "dateCreate", column = "date_create", jdbcType = JdbcType.DATE),
            @Result(property = "dateBegin", column = "date_begin", jdbcType = JdbcType.DATE),
            @Result(property = "dateEnd", column = "date_end", jdbcType = JdbcType.DATE),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "status", column = "status"),
    })
    List<Project> findAll();


    @Insert("INSERT INTO project (id, name, description, date_create, date_begin, date_end, user_id, status) " +
            "VALUES(#{id}, #{name}, #{description}, #{dateCreate}, #{dateBegin}, #{dateEnd}, #{userId}, #{status})")
    void persist(@Param("id")@NotNull final String id,@Param("name") @NotNull final String name,
                 @Param("description")@NotNull final String description,@Param("dateCreate") @NotNull final String dateCreate,
                 @Param("dateBegin") @NotNull final String dateBegin,@Param("dateEnd") @NotNull final String dateEnd,
                 @Param("userId")@NotNull final String userId,@Param("status") @NotNull final String status);


    @Delete("DELETE FROM project")
    void clear();

    @NotNull
    @Select("SELECT COUNT(*) FROM project")
    Long count();

    @NotNull
    @Select("SELECT COUNT(*) = 0 FROM project")
    Boolean isEmpty();

    @NotNull
    @Select("SELECT COUNT(*) > 0 FROM project WHERE id = #{id}")
    Boolean contains(@NotNull final String id);
}
