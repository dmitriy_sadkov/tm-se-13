package ru.sadkov.tm.api;

import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.entity.Task;


import java.util.List;

public interface TaskMapper {
    @NotNull
    @Select("SELECT * FROM task WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "dateCreate", column = "date_create", jdbcType = JdbcType.DATE),
            @Result(property = "dateBegin", column = "date_begin", jdbcType = JdbcType.DATE),
            @Result(property = "dateEnd", column = "date_end", jdbcType = JdbcType.DATE),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "status", column = "status"),
    })
    List<Task> findAllForUser(@NotNull final String userId);

    @Delete("DELETE FROM project WHERE user_id = #{userId}")
    void removeAllForUser(@NotNull final String userId);

    @Nullable
    @Select("SELECT * FROM task WHERE user_id = #{userId} AND id = #{taskId}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "dateCreate", column = "date_create", jdbcType = JdbcType.DATE),
            @Result(property = "dateBegin", column = "date_begin", jdbcType = JdbcType.DATE),
            @Result(property = "dateEnd", column = "date_end", jdbcType = JdbcType.DATE),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "status", column = "status"),
    })
    Task findOne(@Param("taskId") @NotNull final String taskId, @Param("userId") @NotNull final String userId);

    @Update("UPDATE task SET name = #{taskName} WHERE user_id = #{userId} AND id = #{taskId}")
    void update(@Param("taskId") @NotNull final String taskId, @Param("taskName") @NotNull final String taskName, @Param("userId") @NotNull final String userId);

    @Nullable
    @Select("SELECT * FROM task WHERE user_id = #{userId} AND name = #{taskName}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "dateCreate", column = "date_create", jdbcType = JdbcType.DATE),
            @Result(property = "dateBegin", column = "date_begin", jdbcType = JdbcType.DATE),
            @Result(property = "dateEnd", column = "date_end", jdbcType = JdbcType.DATE),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "status", column = "status"),
    })
    Task findTaskByName(@Param("taskName") @NotNull final String taskName, @Param("userId") @NotNull final String userId);

    @Delete("DELETE FROM project WHERE user_id = #{userId} AND name = #{taskName}")
    void removeByName(@Param("taskName") @NotNull final String taskName, @Param("userId") @NotNull final String userId);

    @NotNull
    @Select("SELECT * FROM task WHERE user_id = #{userId} AND name LIKE %#{part}% or description LIKE %#{part}%")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "dateCreate", column = "date_create", jdbcType = JdbcType.DATE),
            @Result(property = "dateBegin", column = "date_begin", jdbcType = JdbcType.DATE),
            @Result(property = "dateEnd", column = "date_end", jdbcType = JdbcType.DATE),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "status", column = "status"),
    })
    List<Task> getTasksByPart(@Param("useId") @NotNull final String userId, @Param("part") @NotNull final String part);

    @NotNull
    @Select("SELECT * FROM task WHERE user_id = #{userId} AND status = #{status}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "dateCreate", column = "date_create", jdbcType = JdbcType.DATE),
            @Result(property = "dateBegin", column = "date_begin", jdbcType = JdbcType.DATE),
            @Result(property = "dateEnd", column = "date_end", jdbcType = JdbcType.DATE),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "status", column = "status"),
    })
    List<Task> findTasksByStatus(@Param("userId") @NotNull final String userId, @Param("status") @NotNull final String status);

    @Update("UPDATE task SET status = #{status}, date_begin = #{dateBegin} WHERE user_id = #{userId} AND name = #{taskName}")
    void startTask(@Param("userId") @NotNull final String userId, @Param("taskName") @NotNull final String taskName,
                   @Param("status") @NotNull final String status, @Param("dateBegin") @NotNull final String dateBegin);

    @Update("UPDATE task SET status = #{status}, date_end = #{dateEnd} WHERE user_id = #{userId} AND name = #{taskName}")
    void endTask(@Param("userId") @NotNull final String userId, @Param("taskName") @NotNull final String taskName,
                 @Param("status") @NotNull final String status, @Param("dateEnd") @NotNull final String dateEnd);

    @NotNull
    @Select("SELECT * FROM task")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "dateCreate", column = "date_create", jdbcType = JdbcType.DATE),
            @Result(property = "dateBegin", column = "date_begin", jdbcType = JdbcType.DATE),
            @Result(property = "dateEnd", column = "date_end", jdbcType = JdbcType.DATE),
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "status", column = "status"),
    })
    List<Task> findAll();

    @Insert("INSERT INTO task (id, name, description, date_create, date_begin, date_end, project_id, user_id, status) " +
            "VALUES(#{id}, #{name}, #{description}, #{dateCreate}, #{dateBegin}, #{dateEnd},#{projectId}, #{userId}, #{status})")
    void persist(@Param("id") @NotNull final String id, @Param("name") @NotNull final String name,
                 @Param("description") @NotNull final String description, @Param("dateCreate") @NotNull final String dateCreate,
                 @Param("dateBegin") @NotNull final String dateBegin, @Param("dateEnd") @NotNull final String dateEnd,
                 @Param("projectId") @NotNull final String projectId, @Param("userId") @NotNull final String userId,
                 @Param("status") @NotNull final String status);

    @Delete("DELETE FROM task")
    void clear();

    @NotNull
    @Select("SELECT COUNT(*) FROM task")
    Long count();

    @NotNull
    @Select("SELECT COUNT(*) = 0 FROM task")
    Boolean isEmpty();

    @NotNull
    @Select("SELECT COUNT(*) > 0 FROM task WHERE id = #{id}")
    Boolean contains(@NotNull final String id);
}
