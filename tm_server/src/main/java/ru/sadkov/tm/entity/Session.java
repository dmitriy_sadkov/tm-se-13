package ru.sadkov.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.enumeration.Role;

import javax.xml.bind.annotation.XmlType;

@Getter
@Setter
@NoArgsConstructor
@XmlType(name = "session")
@JsonIgnoreProperties(ignoreUnknown = true)
public final class Session implements Cloneable {

    @Override
    @Nullable
    public Session clone() {
        try {
            return (Session) super.clone();
        } catch (CloneNotSupportedException ex) {
            return null;
        }
    }

    @NotNull
    private String id;

    @Nullable
    private Long timeStamp;

    @Nullable
    private String userId;

    @Nullable
    private Role role;

    @Nullable
    private String signature;

    @Override
    public String toString() {
        return "Session{" +
                "id='" + id + '\'' +
                ", timeStamp=" + timeStamp +
                ", userId='" + userId + '\'' +
                ", signature='" + signature + '\'' +
                ", role=" + role +
                '}';
    }
}
