package ru.sadkov.tm.service;


import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.sadkov.tm.api.IUserService;
import ru.sadkov.tm.api.UserMapper;
import ru.sadkov.tm.entity.User;
import ru.sadkov.tm.enumeration.Role;


import ru.sadkov.tm.util.HashUtil;
import ru.sadkov.tm.util.MyBatisUtil;


import java.util.ArrayList;
import java.util.List;

public final class UserService extends AbstractService implements IUserService {

    public UserService() {
    }

    public void userRegister(@Nullable final User user) {
        if (user == null) return;
        if (user.getLogin() == null || user.getLogin().isEmpty()) return;
        if (user.getPassword() == null || user.getPassword().isEmpty()) return;
        try (@Nullable final SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()) {
            if (sqlSession == null) return;
            @Nullable final UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
            if (userMapper == null) return;
            @NotNull final String id = user.getId();
            @NotNull final String login = user.getLogin();
            @NotNull final String password = HashUtil.hashMD5(user.getPassword());
            @NotNull final String role = user.getRole().toString();
            userMapper.persist(id, login, password, role);
            sqlSession.commit();
        }
    }

    public void userAddFromData(@Nullable final User user) {
        if (user == null) return;
        try (@Nullable final SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()) {
            if (sqlSession == null) return;
            @Nullable final UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
            if (userMapper == null) return;
            @NotNull final String id = user.getId();
            @NotNull final String login = user.getLogin();
            @NotNull final String password = HashUtil.hashMD5(user.getPassword());
            @NotNull final String role = user.getRole().toString();
            userMapper.persist(id, login, password, role);
            sqlSession.commit();
        }
    }


    public boolean login(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) return false;
        if (password == null || password.isEmpty()) return false;
        try (@Nullable final SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()) {
            if (sqlSession == null) return false;
            @Nullable final UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
            if (userMapper == null) return false;
            @Nullable final User user = userMapper.findByLogin(login);
            if (user == null || user.getPassword() == null) return false;
            return user.getPassword().equals(HashUtil.hashMD5(password));
        }
    }

    @Override
    @NotNull
    public List<User> findAll() {
        try (@Nullable final SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()) {
            if (sqlSession == null) return new ArrayList<>();
            @Nullable final UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
            if (userMapper == null) return new ArrayList<>();
            return userMapper.findAll();
        }
    }

    public void addTestUsers() {
        @NotNull final User admin = new User("admin", "admin", Role.ADMIN);
        @NotNull final User user = new User("user", "user", Role.USER);
        userRegister(admin);
        userRegister(user);
    }

    @Override
    public void clear() {
        try (@Nullable final SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()) {
            if (sqlSession == null) return;
            @Nullable final UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
            if (userMapper == null) return;
            userMapper.removeAll();
            sqlSession.commit();
        }
    }

    @Override
    public void load(@Nullable final List<User> users) {
        if (users == null) return;
        clear();
        for (@NotNull final User user : users) {
            userRegister(user);
        }
    }


    @Override
    @Nullable
    public User findOneById(@NotNull String userId) {
        try (@Nullable final SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()) {
            if (sqlSession == null) return null;
            @Nullable final UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
            if (userMapper == null) return null;
            return userMapper.findOne(userId);
        }
    }

    @Override
    @Nullable
    public User findOneByLogin(@Nullable final String login) {
        try (@Nullable final SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()) {
            if (sqlSession == null) return null;
            @Nullable final UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
            if (userMapper == null) return null;
            return userMapper.findByLogin(login);
        }
    }
}
