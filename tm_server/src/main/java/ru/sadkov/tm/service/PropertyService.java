package ru.sadkov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.api.IPropertyService;

import java.io.InputStream;
import java.util.Properties;

public final class PropertyService implements IPropertyService {

    @NotNull private final String NAME = "/application.properties";

    @NotNull private final Properties properties = new Properties();

    public void init() throws Exception{
        @NotNull final InputStream inputStream = PropertyService.class.getResourceAsStream(NAME);
        properties.load(inputStream);
    }

    @NotNull
    public String getServerHost(){
        @NotNull final String propertyHost = properties.getProperty("server.host");
        @Nullable final String envHost = System.getProperty("server.host");
        if(envHost==null)return propertyHost;
        return envHost;
    }

    @NotNull
    public String getServerPort(){
        @NotNull final String propertyPort = properties.getProperty("server.port");
        @Nullable final String envPort = System.getProperty("server.port");
        if(envPort==null)return propertyPort;
        return envPort;
    }

    @NotNull
    public String getSessionSalt(){
        @Nullable final String propertySessionSalt = properties.getProperty("session.salt");
        if(propertySessionSalt==null||propertySessionSalt.isEmpty()) return "HardSalt";
        return propertySessionSalt;
    }

    @NotNull
    public Integer getSessionCycle(){
        @Nullable final String propertySessionCycle = properties.getProperty("session.cycle");
        if(propertySessionCycle==null||propertySessionCycle.isEmpty()) return 824;
        try{
            return Integer.parseInt(propertySessionCycle);
        }catch (Exception e){
            return 824;
        }
    }
}
