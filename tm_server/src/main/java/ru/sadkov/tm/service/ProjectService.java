package ru.sadkov.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.api.IProjectService;
import ru.sadkov.tm.api.ProjectMapper;
import ru.sadkov.tm.entity.Project;
import ru.sadkov.tm.entity.User;
import ru.sadkov.tm.enumeration.Status;
import ru.sadkov.tm.util.MyBatisUtil;
import ru.sadkov.tm.util.RandomUtil;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public final class ProjectService extends AbstractService implements IProjectService {

    public ProjectService() {
    }

    @Override
    public List<Project> getSortedProjectList(@Nullable final String userId, @Nullable final Comparator<Project> comparator) {
        if (userId == null || userId.isEmpty()) return null;
        if (comparator == null) return null;
        try (@Nullable final SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()) {
            if (sqlSession == null) return null;
            @Nullable final ProjectMapper projectMapper = sqlSession.getMapper(ProjectMapper.class);
            if (projectMapper == null) return null;
            @NotNull final List<Project> projects = projectMapper.findAllForUser(userId);
            projects.sort(comparator);
            return projects;
        }
    }

    @Nullable
    public String findProjectIdByName(@Nullable final String projectName, @Nullable final String userId) {
        if (projectName == null || projectName.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        try (@Nullable final SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()) {
            if (sqlSession == null) return null;
            @Nullable final ProjectMapper projectMapper = sqlSession.getMapper(ProjectMapper.class);
            if (projectMapper == null) return null;
            @Nullable final Project project = projectMapper.findProjectByName(projectName, userId);
            if (project == null) return null;
            return project.getId();
        }
    }

    public boolean persist(@Nullable final String projectName, @Nullable final String userId, @Nullable final String description) {
        if (projectName == null || projectName.isEmpty()) return false;
        if (userId == null || userId.isEmpty()) return false;
        if (description == null || description.isEmpty()) return false;
        try (@Nullable final SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()) {
            if (sqlSession == null) return false;
            @Nullable final ProjectMapper projectMapper = sqlSession.getMapper(ProjectMapper.class);
            if (projectMapper == null) return false;
            @Nullable Project project = projectMapper.findProjectByName(projectName, userId);
            if (project == null) {
                project = new Project(projectName, RandomUtil.UUID(), userId, description);
                projectMapper.persist(project.getId(), projectName, description,
                        simpleDateFormatForDB.format(project.getDateCreate()), simpleDateFormatForDB.format(project.getDateBegin()),
                        simpleDateFormatForDB.format(project.getDateEnd()), userId, project.getStatus().toString());
                sqlSession.commit();
                return true;
            }
            return false;
        }
    }

    public void removeByName(@Nullable final String projectName, @Nullable final String userId) {
        if (projectName == null || projectName.isEmpty() || userId == null || userId.isEmpty()) return;
        @Nullable final String projectId = findProjectIdByName(projectName, userId);
        if (projectId == null || projectId.isEmpty()) return;
        try (@Nullable final SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()) {
            if (sqlSession == null) return;
            @Nullable final ProjectMapper projectMapper = sqlSession.getMapper(ProjectMapper.class);
            if (projectMapper == null) return;
            projectMapper.removeByName(userId, projectName);
            sqlSession.commit();
        }
    }

    @Nullable
    public List<Project> findAll(@Nullable final User user) {
        if (user == null) return null;
        try (@Nullable final SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()) {
            if (sqlSession == null) return null;
            @Nullable final ProjectMapper projectMapper = sqlSession.getMapper(ProjectMapper.class);
            if (projectMapper == null) return null;
            return projectMapper.findAllForUser(user.getId());
        }
    }

    public void removeAll(@Nullable final User user) {
        if (user == null) return;
        try (@Nullable final SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()) {
            if (sqlSession == null) return;
            @Nullable final ProjectMapper projectMapper = sqlSession.getMapper(ProjectMapper.class);
            if (projectMapper == null) return;
            projectMapper.removeAll(user.getId());
        }
    }

    public void update(@Nullable final Project project, @Nullable final String projectName, @Nullable final String description) {
        if (project == null || projectName == null || projectName.isEmpty()) return;
        if (description == null || description.isEmpty()) return;
        try (@Nullable final SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()) {
            if (sqlSession == null) return;
            @Nullable final ProjectMapper projectMapper = sqlSession.getMapper(ProjectMapper.class);
            if (projectMapper == null) return;
            projectMapper.update(project.getUserId(), project.getId(), description, projectName);
            sqlSession.commit();
        }
    }

    @Nullable
    public Project findOneByName(@Nullable final String projectName, @Nullable final User currentUser) {
        if (projectName == null || projectName.isEmpty() || currentUser == null) return null;
        @Nullable final String projectId = findProjectIdByName(projectName, currentUser.getId());
        if (projectId == null || projectId.isEmpty()) return null;
        try (@Nullable final SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()) {
            if (sqlSession == null) return null;
            @Nullable final ProjectMapper projectMapper = sqlSession.getMapper(ProjectMapper.class);
            if (projectMapper == null) return null;
            return projectMapper.findProjectByName(projectName, currentUser.getId());
        }
    }

    @Override
    @Nullable
    public List<Project> findProjectsByPart(@Nullable final String userId, @Nullable final String part) {
        if (userId == null || userId.isEmpty()) return null;
        if (part == null || part.isEmpty()) return null;
        try (@Nullable final SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()) {
            if (sqlSession == null) return null;
            @Nullable final ProjectMapper projectMapper = sqlSession.getMapper(ProjectMapper.class);
            if (projectMapper == null) return null;
            return projectMapper.findProjectsByPart(userId, part);
        }
    }

    @Override
    @Nullable
    public List<Project> findProjectsByStatus(@Nullable final String userId, @Nullable final Status status) {
        if (userId == null || userId.isEmpty() || status == null) return null;
        try (@Nullable final SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()) {
            if (sqlSession == null) return null;
            @Nullable final ProjectMapper projectMapper = sqlSession.getMapper(ProjectMapper.class);
            if (projectMapper == null) return null;
            return projectMapper.findProjectsByStatus(userId, status.toString());
        }
    }

    @Override
    @Nullable
    public String startProject(@Nullable final String userId, @Nullable final String projectName) {
        if (userId == null || userId.isEmpty()) return null;
        if (projectName == null || projectName.isEmpty()) return null;
        try (@Nullable final SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()) {
            if (sqlSession == null) return null;
            @Nullable final ProjectMapper projectMapper = sqlSession.getMapper(ProjectMapper.class);
            if (projectMapper == null) return null;
            @NotNull final Date startDate = new Date();
            projectMapper.startProject(userId, projectName, Status.PROCESS.toString(), simpleDateFormatForDB.format(startDate));
            sqlSession.commit();
            return simpleDateFormat.format(startDate);
        }
    }

    @Override
    public @Nullable String endProject(@Nullable final String userId, @Nullable final String projectName) {
        if (userId == null || userId.isEmpty()) return null;
        if (projectName == null || projectName.isEmpty()) return null;
        try (@Nullable final SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()) {
            if (sqlSession == null) return null;
            @Nullable final ProjectMapper projectMapper = sqlSession.getMapper(ProjectMapper.class);
            if (projectMapper == null) return null;
            @NotNull final Date endDate = new Date();
            projectMapper.endProject(userId, projectName, Status.DONE.toString(), simpleDateFormatForDB.format(endDate));
            sqlSession.commit();
            return simpleDateFormat.format(endDate);
        }
    }

    @Override
    @NotNull
    public List<Project> findAll(@NotNull final List<User> userList) {
        try (@Nullable final SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()) {
            if (sqlSession == null) return new ArrayList<>();
            @Nullable final ProjectMapper projectMapper = sqlSession.getMapper(ProjectMapper.class);
            if (projectMapper == null) return new ArrayList<>();
            return projectMapper.findAll();
        }
    }

    @Override
    public void persist(@Nullable final Project project) {
        if (project == null) return;
        try (@Nullable final SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()) {
            if (sqlSession == null) return;
            @Nullable final ProjectMapper projectMapper = sqlSession.getMapper(ProjectMapper.class);
            if (projectMapper == null) return;
            projectMapper.persist(project.getId(), project.getName(), project.getDescription(),
                    simpleDateFormatForDB.format(project.getDateCreate()), simpleDateFormatForDB.format(project.getDateBegin()),
                    simpleDateFormatForDB.format(project.getDateEnd()), project.getUserId(), project.getStatus().toString());
            sqlSession.commit();
        }
    }

    @Override
    public void clear() {
        try (@Nullable final SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()) {
            if (sqlSession == null) return;
            @Nullable final ProjectMapper projectMapper = sqlSession.getMapper(ProjectMapper.class);
            if (projectMapper == null) return;
            projectMapper.clear();
            sqlSession.commit();
        }
    }

    @Override
    public void load(@Nullable final List<Project> projects) {
        if (projects == null) return;
        clear();
        for (@NotNull final Project project : projects) {
            persist(project);
        }
    }
}
