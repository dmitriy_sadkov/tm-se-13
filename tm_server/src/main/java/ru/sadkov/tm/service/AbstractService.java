package ru.sadkov.tm.service;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;

public abstract class AbstractService {

    @NotNull
    protected SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");

    protected final SimpleDateFormat simpleDateFormatForDB = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
}
