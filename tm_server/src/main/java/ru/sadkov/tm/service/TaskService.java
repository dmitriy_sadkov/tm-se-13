package ru.sadkov.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.api.IProjectService;

import ru.sadkov.tm.api.ITaskService;
import ru.sadkov.tm.api.TaskMapper;

import ru.sadkov.tm.entity.Project;
import ru.sadkov.tm.entity.Task;
import ru.sadkov.tm.entity.User;
import ru.sadkov.tm.enumeration.Status;

import ru.sadkov.tm.util.MyBatisUtil;
import ru.sadkov.tm.util.RandomUtil;


import java.util.*;

public final class TaskService extends AbstractService implements ITaskService {

    @NotNull
    private IProjectService projectService;

    public TaskService(@NotNull final IProjectService projectService) {
        this.projectService = projectService;
    }

    @Nullable
    public Task findTaskByName(@Nullable final String taskName, @Nullable final String userId) {
        if (taskName == null || taskName.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        try (@Nullable final SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()) {
            if (sqlSession == null) return null;
            @Nullable final TaskMapper taskMapper = sqlSession.getMapper(TaskMapper.class);
            if (taskMapper == null) return null;
            return taskMapper.findTaskByName(taskName, userId);
        }
    }

    @Nullable
    public List<Task> getSortedTaskList(@Nullable final String userId, @Nullable final Comparator<Task> comparator) {
        if (userId == null || userId.isEmpty()) return null;
        if (comparator == null) return null;
        try (@Nullable final SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()) {
            if (sqlSession == null) return null;
            @Nullable final TaskMapper taskMapper = sqlSession.getMapper(TaskMapper.class);
            if (taskMapper == null) return null;
            @NotNull final List<Task> tasks = taskMapper.findAllForUser(userId);
            tasks.sort(comparator);
            return tasks;
        }
    }

    public boolean saveTask(@Nullable final String taskName, @Nullable final String projectName, @Nullable final User currentUser) {
        if (taskName == null || taskName.isEmpty()) return false;
        if (projectName == null || projectName.isEmpty()) return false;
        if (currentUser == null) return false;
        @Nullable final String projectId = projectService.findProjectIdByName(projectName, currentUser.getId());
        if (projectId == null || projectId.isEmpty()) return false;
        try (@Nullable final SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()) {
            if (sqlSession == null) return false;
            @Nullable final TaskMapper taskMapper = sqlSession.getMapper(TaskMapper.class);
            if (taskMapper == null) return false;
            @Nullable Task task = taskMapper.findTaskByName(taskName, currentUser.getId());
            if (task == null) {
                task = new Task(taskName, RandomUtil.UUID(), projectId, currentUser.getId());
                persist(task);
                sqlSession.commit();
                return true;
            }
            taskMapper.update(task.getId(), taskName, currentUser.getId());
            sqlSession.commit();
            return true;
        }
    }

    public void removeTask(@Nullable final String taskName, @Nullable final User currentUser) {
        if (taskName == null || taskName.isEmpty()) return;
        if (currentUser == null) return;
        try (@Nullable final SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()) {
            if (sqlSession == null) return;
            @Nullable final TaskMapper taskMapper = sqlSession.getMapper(TaskMapper.class);
            if (taskMapper == null) return;
            taskMapper.removeByName(taskName, currentUser.getId());
            sqlSession.commit();
        }
    }

    @Nullable
    public List<Task> findAll(@Nullable final User user) {
        if (user == null) return null;
        try (@Nullable final SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()) {
            if (sqlSession == null) return null;
            @Nullable final TaskMapper taskMapper = sqlSession.getMapper(TaskMapper.class);
            if (taskMapper == null) return null;
            return taskMapper.findAllForUser(user.getId());
        }
    }


    public void removeAll(@Nullable final User user) {
        if (user == null) return;
        try (@Nullable final SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()) {
            if (sqlSession == null) return;
            @Nullable final TaskMapper taskMapper = sqlSession.getMapper(TaskMapper.class);
            if (taskMapper == null) return;
            taskMapper.removeAllForUser(user.getId());
            sqlSession.commit();
        }
    }

    public void removeTaskForProject(@Nullable final String projectName, @Nullable final User currentUser) {
        if (projectName == null || projectName.isEmpty() || currentUser == null) return;
        @Nullable final String projectId = projectService.findProjectIdByName(projectName, currentUser.getId());
        if (projectId == null || projectId.isEmpty()) return;
        try (@Nullable final SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()) {
            if (sqlSession == null) return;
            @Nullable final TaskMapper taskMapper = sqlSession.getMapper(TaskMapper.class);
            if (taskMapper == null) return;
            @Nullable final Project project = projectService.findOneByName(projectName, currentUser);
            if (project == null) return;
            @NotNull final Iterator<Task> iterator = taskMapper.findAllForUser(currentUser.getId()).iterator();
            while (iterator.hasNext()) {
                @NotNull final Task task = iterator.next();
                if (task.getProjectId().equals(projectId)) {
                    taskMapper.removeByName(task.getName(), currentUser.getId());
                    sqlSession.commit();
                }
            }
        }
    }

    public void update(@Nullable final String oldName, @Nullable final String newName, @Nullable final String userId) {
        if (oldName == null || oldName.isEmpty()) return;
        if (newName == null || newName.isEmpty()) return;
        if (userId == null || userId.isEmpty()) return;
        try (@Nullable final SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()) {
            if (sqlSession == null) return;
            @Nullable final TaskMapper taskMapper = sqlSession.getMapper(TaskMapper.class);
            if (taskMapper == null) return;
            @Nullable final Task task = taskMapper.findTaskByName(oldName, userId);
            if (task == null) return;
            taskMapper.update(task.getId(), newName, userId);
            sqlSession.commit();
        }
    }

    @Override
    @Nullable
    public List<Task> getTasksByPart(@Nullable final String userId, @Nullable final String part) {
        if (userId == null || userId.isEmpty()) return null;
        if (part == null || part.isEmpty()) return null;
        try (@Nullable final SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()) {
            if (sqlSession == null) return null;
            @Nullable final TaskMapper taskMapper = sqlSession.getMapper(TaskMapper.class);
            if (taskMapper == null) return null;
            return taskMapper.getTasksByPart(userId, part);
        }
    }

    @Override
    @Nullable
    public List<Task> findTasksByStatus(@Nullable final String userId, @Nullable final Status status) {
        if (userId == null || userId.isEmpty() || status == null) return null;
        try (@Nullable final SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()) {
            if (sqlSession == null) return null;
            @Nullable final TaskMapper taskMapper = sqlSession.getMapper(TaskMapper.class);
            if (taskMapper == null) return null;
            return taskMapper.findTasksByStatus(userId, status.toString());
        }
    }

    @Override
    @Nullable
    public String startTask(@Nullable final String userId, @Nullable final String taskName) {
        if (userId == null || userId.isEmpty()) return null;
        if (taskName == null || taskName.isEmpty()) return null;
        try (@Nullable final SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()) {
            if (sqlSession == null) return null;
            @Nullable final TaskMapper taskMapper = sqlSession.getMapper(TaskMapper.class);
            if (taskMapper == null) return null;
            @NotNull final Date startDate = new Date();
            taskMapper.startTask(userId, taskName, Status.PROCESS.toString(), simpleDateFormatForDB.format(startDate));
            sqlSession.commit();
            return simpleDateFormat.format(startDate);
        }
    }

    @Override
    @Nullable
    public String endTask(@Nullable final String userId, @Nullable final String taskName) {
        if (userId == null || userId.isEmpty()) return null;
        if (taskName == null || taskName.isEmpty()) return null;
        try (@Nullable final SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()) {
            if (sqlSession == null) return null;
            @Nullable final TaskMapper taskMapper = sqlSession.getMapper(TaskMapper.class);
            if (taskMapper == null) return null;
            @NotNull final Date endDate = new Date();
            taskMapper.endTask(userId, taskName, Status.DONE.toString(), simpleDateFormatForDB.format(endDate));
            sqlSession.commit();
            return simpleDateFormat.format(endDate);
        }
    }

    @Override
    @NotNull
    public List<Task> findAll(@NotNull final List<User> userList) {
        try (@Nullable final SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()) {
            if (sqlSession == null) return new ArrayList<>();
            @Nullable final TaskMapper taskMapper = sqlSession.getMapper(TaskMapper.class);
            if (taskMapper == null) return new ArrayList<>();
            return taskMapper.findAll();
        }
    }

    @Override
    public void persist(@Nullable final Task task) {
        if (task == null) return;
        try (@Nullable final SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()) {
            if (sqlSession == null) return;
            @Nullable final TaskMapper taskMapper = sqlSession.getMapper(TaskMapper.class);
            if (taskMapper == null) return;
            taskMapper.persist(task.getId(), task.getName(),
                    task.getDescription(), simpleDateFormatForDB.format(task.getDateCreate()),
                    simpleDateFormatForDB.format(task.getDateBegin()), simpleDateFormatForDB.format(task.getDateEnd()),
                    task.getProjectId(), task.getUserId(), task.getStatus().toString());
            sqlSession.commit();
        }
    }

    @Override
    public void clear() {
        try (@Nullable final SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()) {
            if (sqlSession == null) return;
            @Nullable final TaskMapper taskMapper = sqlSession.getMapper(TaskMapper.class);
            if (taskMapper == null) return;
            taskMapper.clear();
            sqlSession.commit();
        }
    }

    @Override
    public void load(@Nullable final List<Task> tasks) {
        if (tasks == null) return;
        clear();
        try (@Nullable final SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession()) {
            if (sqlSession == null) return;
            @Nullable final TaskMapper taskMapper = sqlSession.getMapper(TaskMapper.class);
            if (taskMapper == null) return;
            for (@NotNull final Task task : tasks) {
                taskMapper.persist(task.getId(), task.getName(),
                        task.getDescription(), simpleDateFormatForDB.format(task.getDateCreate()),
                        simpleDateFormatForDB.format(task.getDateBegin()), simpleDateFormatForDB.format(task.getDateEnd()),
                        task.getProjectId(), task.getUserId(), task.getStatus().toString());
                sqlSession.commit();
            }
        }
    }
}

