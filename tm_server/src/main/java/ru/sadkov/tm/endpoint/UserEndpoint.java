package ru.sadkov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.api.ServiceLocator;
import ru.sadkov.tm.entity.Session;
import ru.sadkov.tm.entity.User;
import ru.sadkov.tm.enumeration.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.security.NoSuchAlgorithmException;
import java.util.List;

@WebService
public final class UserEndpoint {

    @NotNull
    private final ServiceLocator serviceLocator;

    public UserEndpoint(@NotNull ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Nullable
    @WebMethod
    public User getCurrentUser(
            @WebParam(name = "session") @Nullable final Session session) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getSessionService().getUser(session);
    }


    @WebMethod
    public void userRegister(
            @WebParam(name = "user") @Nullable final User user
    ) throws NoSuchAlgorithmException {
        serviceLocator.getUserService().userRegister(user);
    }

    @WebMethod
    public void userAddFromData(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "user") @Nullable final User user) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        if (user == null) return;
        serviceLocator.getUserService().userAddFromData(user);
    }

    @WebMethod
    public Session login(
            @WebParam(name = "login") @Nullable final String login,
            @WebParam(name = "password") @Nullable final String password
    ){
        return serviceLocator.getSessionService().open(login, password);
    }


    @NotNull
    @WebMethod
    public List<User> findAllUsers(
            @WebParam(name = "session") @Nullable final Session session) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().findAll();
    }

    @WebMethod
    public void logout(
            @WebParam(name = "session") @Nullable final Session session) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getSessionService().close(session);
    }

    @WebMethod
    public void updatePassword(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "newPassword") @Nullable final String newPassword) throws Exception, NoSuchAlgorithmException {
        serviceLocator.getSessionService().validate(session);
        //serviceLocator.getUserService().updatePassword(newPassword);
    }

    @Nullable
    @WebMethod
    public User findOneUser(
            @WebParam(name = "session") @Nullable final Session session) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getSessionService().getUser(session);
    }

    @WebMethod
    public boolean updateProfile(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "newUserName") @Nullable final String newUserName) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return true;//serviceLocator.getUserService().updateProfile(newUserName);
    }

    @WebMethod
    public void addTestUsers() throws NoSuchAlgorithmException {
        serviceLocator.getUserService().addTestUsers();
    }

    @WebMethod
    public boolean isAuth(
            @WebParam(name = "session") @Nullable final Session session) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getSessionService().isValid(session);
    }

    @WebMethod
    public void clearUsers(
            @WebParam(name = "session") @Nullable final Session session) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getUserService().clear();
    }

    @WebMethod
    public void loadUsers(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "users") @Nullable final List<User> users) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getUserService().load(users);
    }
}
