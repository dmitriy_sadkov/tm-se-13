package ru.sadkov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.endpoint.Exception_Exception;
import ru.sadkov.tm.endpoint.Task;
import ru.sadkov.tm.exception.WrongDataException;
import ru.sadkov.tm.util.ListShowUtil;

import java.util.ArrayList;
import java.util.List;

public final class TaskForProjectCommand extends AbstractCommand {

    @Override
    public String command() {
        return "tasks-for-project";
    }

    @Override
    public String description() {
        return "Show tasks for chosen project";
    }

    @Override
    public void execute() throws WrongDataException, Exception_Exception {
        System.out.println("[TASKS FOR PROJECT]");
        System.out.println("[ENTER PROJECT NAME]");
        @Nullable final String projectName = serviceLocator.getScanner().nextLine();
        if (projectName == null || projectName.isEmpty() || serviceLocator.getCurrentSession() == null)
            throw new WrongDataException();
        @Nullable final String projectId = serviceLocator.getProjectEndpoint()
                .findProjectIdByName(serviceLocator.getCurrentSession(),projectName);
        if (projectId == null || projectId.isEmpty()) throw new WrongDataException();
        @NotNull final List<Task> result = new ArrayList<>();
        @Nullable final List<Task> tasksForUser = serviceLocator.getTaskEndpoint()
                .findAllTasksForUser(serviceLocator.getCurrentSession());
        if (tasksForUser == null || tasksForUser.isEmpty()) throw new WrongDataException();
        for (@NotNull final Task task : tasksForUser) {
            if (task.getProjectId() != null && task.getProjectId().equals(projectId)) {
                result.add(task);
            }
        }
        if (result.isEmpty()) throw new WrongDataException();
        ListShowUtil.showList(result);
    }


    @Override
    public boolean safe() {
        return false;
    }
}
