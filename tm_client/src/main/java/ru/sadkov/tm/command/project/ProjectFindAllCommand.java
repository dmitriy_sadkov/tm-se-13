package ru.sadkov.tm.command.project;

import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.endpoint.Exception_Exception;
import ru.sadkov.tm.endpoint.Project;
import ru.sadkov.tm.exception.WrongDataException;
import ru.sadkov.tm.util.ListShowUtil;

import java.util.List;

public final class ProjectFindAllCommand extends AbstractCommand {

    @Override
    public String command() {
        return "project-list";
    }

    @Override
    public String description() {
        return "Show all projects";
    }

    @Override
    public void execute() throws WrongDataException, Exception_Exception {
        @Nullable final List<Project> projectList = serviceLocator.getProjectEndpoint()
                .findAllProjectsForUser(serviceLocator.getCurrentSession());
        if (projectList == null || projectList.isEmpty()) throw new WrongDataException("NO PROJECTS");
        System.out.println("[PROJECTS:]");
        ListShowUtil.showList(projectList);
    }

    @Override
    public boolean safe() {
        return false;
    }
}
