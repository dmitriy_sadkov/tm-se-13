package ru.sadkov.tm.command.data;


import ru.sadkov.tm.command.AbstractCommand;


public final class DomainSaveJSONCommand extends AbstractCommand {

    @Override
    public String command() {
        return "save-JSON";
    }

    @Override
    public String description() {
        return "Save domain in JSON";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[SAVING TO FILE]");
        if(serviceLocator.getDomainEndpoint().domainSaveJSON(serviceLocator.getCurrentSession())) return;
        System.out.println("ACCESS FORBIDDEN");
    }

    @Override
    public boolean safe() {
        return true;
    }
}
