package ru.sadkov.tm.command.data;
import ru.sadkov.tm.command.AbstractCommand;

public final class DomainLoadFromJSONCommand extends AbstractCommand {

    @Override
    public String command() {
        return "load-JSON";
    }

    @Override
    public String description() {
        return "Load domain from in JSON";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[READING FROM JSON]");
        if(serviceLocator.getDomainEndpoint().domainLoadJSON(serviceLocator.getCurrentSession())) return;
        System.out.println("ACCESS FORBIDDEN");
    }

    @Override
    public boolean safe() {
        return true;
    }
}
