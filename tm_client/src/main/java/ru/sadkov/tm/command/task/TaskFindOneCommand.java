package ru.sadkov.tm.command.task;

import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.endpoint.Exception_Exception;
import ru.sadkov.tm.endpoint.Task;
import ru.sadkov.tm.exception.WrongDataException;

public final class TaskFindOneCommand extends AbstractCommand {

    @Override
    public String command() {
        return "task-show";
    }

    @Override
    public String description() {
        return "Show task";
    }

    @Override
    public void execute() throws WrongDataException, Exception_Exception {
        System.out.println("[SHOW TASK]");
        System.out.println("[ENTER TASK NAME]");
        @Nullable final String taskName = serviceLocator.getScanner().nextLine();
        if (taskName == null || taskName.isEmpty() || serviceLocator.getCurrentSession() == null)
            throw new WrongDataException("[INCORRECT NAME]");
        @Nullable final Task task = serviceLocator.getTaskEndpoint()
                .findTaskByName(serviceLocator.getCurrentSession(),taskName);
        if (task == null) throw new WrongDataException("[NO SUCH TASK]");
        System.out.println(task);
    }

    @Override
    public boolean safe() {
        return false;
    }
}
