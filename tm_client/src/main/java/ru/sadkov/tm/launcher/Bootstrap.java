package ru.sadkov.tm.launcher;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.sadkov.tm.api.ServiceLocator;
import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.endpoint.*;

import java.lang.Exception;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final Scanner scanner = new Scanner(System.in);

    @NotNull
    private final DomainEndpoint domainEndpoint = new DomainEndpointService().getDomainEndpointPort();

    @NotNull
    private final ProjectEndpoint projectEndpoint = new ProjectEndpointService().getProjectEndpointPort();

    @NotNull
    private final TaskEndpoint taskEndpoint = new TaskEndpointService().getTaskEndpointPort();

    @NotNull
    private final UserEndpoint userEndpoint = new UserEndpointService().getUserEndpointPort();

    @NotNull
    private final SessionEndpoint sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();

    @Nullable
    private Session currentSession = null;

    @Nullable
    private static final Set<Class<? extends AbstractCommand>> classes =
            new Reflections("ru.sadkov.tm.command").getSubTypesOf(AbstractCommand.class);

    @NotNull
    private final Map<String, AbstractCommand> commandMap = new LinkedHashMap<>();

    @NotNull
    public Map<String, AbstractCommand> getCommandMap() {
        return commandMap;
    }

    @NotNull
    public SessionEndpoint getSessionEndpoint() {
        return sessionEndpoint;
    }

    @NotNull
    public Scanner getScanner() {
        return scanner;
    }

    @Nullable
    public Session getCurrentSession() {
        return currentSession;
    }

    @Override
    public void setCurrentSession(@Nullable final Session currentSession) {
        this.currentSession = currentSession;
    }

    @NotNull
    public DomainEndpoint getDomainEndpoint() {
        return domainEndpoint;
    }

    @NotNull
    public ProjectEndpoint getProjectEndpoint() {
        return projectEndpoint;
    }

    @NotNull
    public TaskEndpoint getTaskEndpoint() {
        return taskEndpoint;
    }

    @NotNull
    public UserEndpoint getUserEndpoint() {
        return userEndpoint;
    }

    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) throws IllegalAccessException, InstantiationException {
        @Nullable final AbstractCommand abstractCommand = clazz.newInstance();
        if (abstractCommand == null) return;
        @Nullable final String commandName = abstractCommand.command();
        @Nullable final String commandDescription = abstractCommand.description();
        if (commandName == null || commandName.isEmpty()) return;
        if (commandDescription == null || commandDescription.isEmpty()) return;
        abstractCommand.setServiceLocator(this);
        commandMap.put(commandName, abstractCommand);
    }

    private void start() {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        System.out.println("enter HELP for command list");
        String command = "";
        while (!"exit".equals(command)) {
            command = scanner.nextLine();
            try {
                execute(command);
                System.out.println("[DONE]");
                System.out.println("-----------------------------------");
            } catch (Exception e) {
                System.out.println(e.getMessage());
                System.out.println("-----------------------------------");
            }
        }
    }

    private void execute(@Nullable final String command) throws Exception {
        if (command == null || command.isEmpty()) return;
        @Nullable final AbstractCommand abstractCommand = commandMap.get(command);
        if (abstractCommand == null) return;
        boolean a = abstractCommand.safe();
        boolean b = sessionEndpoint.isValid(currentSession);
        if (abstractCommand.safe() || (sessionEndpoint.isValid(currentSession))) {
            abstractCommand.execute();
            return;
        }
        System.out.println("[ACCESS DENIED]");
    }

    public void init() {
        if (classes == null || classes.isEmpty()) return;
        try {
            for (@NotNull final Class<? extends AbstractCommand> clazz : classes) {
                registry(clazz);
            }
            start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
